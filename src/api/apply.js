import request from '@/utils/request'

export function startApply(data) {
  return request({
    url: '/bpm/apply/startProcess',
    method: 'post',
    data
  })
}

export function saveDraftApply(data) {
  return request({
    url: '/bpm/apply/saveDraftApply',
    method: 'post',
    data
  })
}

export function getApplyDetail(data) {
  return request({
    url: '/bpm/apply/getApplyDetail/' + data,
    method: 'post'
  })
}

export function getApproveDetail(data) {
  return request({
    url: '/bpm/apply/getApproveDetail/' + data,
    method: 'post'
  })
}

export function getApplyLineChart(data) {
  return request({
    url: '/bpm/apply/getApplyLineChart',
    method: 'post',
    data
  })
}

